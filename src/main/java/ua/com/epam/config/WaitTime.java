package ua.com.epam.config;

public interface WaitTime {
    int IMPLICITLY_TIME = 50;
    int WAIT_WITH_TIMEOUT = 60;
    int POLLING_EVERY_TIME = 5;
}
