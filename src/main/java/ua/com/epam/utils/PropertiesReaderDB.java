package ua.com.epam.utils;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Log4j2
public abstract class PropertiesReaderDB {
    protected Properties props = new Properties();

    public PropertiesReaderDB() {
        InputStream fileProp;

        try {
            fileProp = PropertiesReaderDB.class.getClassLoader().getResourceAsStream("gmaildata.properties");
            props.load(fileProp);
            log.info("Successfully read gmaildata.properties.");
        } catch (IOException e) {
            log.error("Can't to read the gmaildata.properties.", e);
        }
    }
}
