package ua.com.epam.utils;


import static ua.com.epam.config.GmailProperties.*;

public class DataPropGmail extends PropertiesReaderDB {

    public String getGmailPage() {
        return props.getProperty(GMAIL_LOGIN_PAGE);
    }

    public String getRecipient() {
        return props.getProperty(RECIPIENT);
    }

    public String getTopic() {
        return props.getProperty(TOPIC);
    }
}
