package ua.com.epam.businessobject;

import lombok.extern.log4j.Log4j2;
import ua.com.epam.pageobject.GmailLoginPage;

@Log4j2
public class GmailLoginBO {
    private final GmailLoginPage gmailHomePage;

    public GmailLoginBO() {
        this.gmailHomePage = new GmailLoginPage();
    }

    public GmailHomeBO loginToGmail(String login, String password) {
        log.info("Setting login - {}.", login);
        gmailHomePage.getSetLogin().sendKeysAfterElementToBeClickable(login);
        gmailHomePage.getSubmitButtonLogin().getWrappedElement().click();

        log.info("Setting password.");
        gmailHomePage.getSetPassword().sendKeysAfterElementToBeClickable(password);
        gmailHomePage.getSubmitButtonPassword().getWrappedElement().click();

        return new GmailHomeBO();
    }
}
