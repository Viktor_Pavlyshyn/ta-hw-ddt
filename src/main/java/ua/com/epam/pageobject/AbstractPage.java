package ua.com.epam.pageobject;

import org.openqa.selenium.support.PageFactory;
import ua.com.epam.factorypage.CustomFieldDecorator;
import ua.com.epam.webdriver.LocalDriverManager;

public abstract class AbstractPage {

    public AbstractPage() {
        PageFactory.initElements(new CustomFieldDecorator(LocalDriverManager.getWebDriver()), this);
    }
}
