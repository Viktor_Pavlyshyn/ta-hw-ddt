package ua.com.epam.pageobject;

import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ua.com.epam.elementdecorator.ButtonElement;
import ua.com.epam.elementdecorator.CheckboxElement;
import ua.com.epam.elementdecorator.LinkElement;
import ua.com.epam.elementdecorator.TabElement;

@Getter
public class SentMessagePage extends AbstractPage {
    final private String firstMessage = "//tr[1][ancestor::div[@class='ae4 UI']]//span[@class='y2']";

    @FindBy(xpath = firstMessage)
    private TabElement sentMessage;
    @FindBy(xpath = "//div[@gh='tm']//span[@class='T-Jo J-J5-Ji']")
    private CheckboxElement allCheckbox;
    @FindBy(xpath = "//div[@gh='tm']//div[@class='T-I J-J5-Ji nX T-I-ax7 T-I-Js-Gs mA'][descendant::div[@class='asa' and div[@class='ar9 T-I-J3 J-J5-Ji']]]")
    private ButtonElement deleteMsg;
    @FindBy(xpath = "//span[parent::td[@class='TC']]")
    private LinkElement refSendInClearAreaMsg;

    public SentMessagePage() {
        super();
    }
}
