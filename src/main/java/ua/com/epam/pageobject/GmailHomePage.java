package ua.com.epam.pageobject;

import lombok.Getter;
import org.openqa.selenium.support.FindBy;
import ua.com.epam.elementdecorator.ButtonElement;
import ua.com.epam.elementdecorator.InputElement;
import ua.com.epam.elementdecorator.TabElement;

@Getter
public class GmailHomePage extends AbstractPage {
    protected final String sentMsgTabXpath = "//a[contains(@href,'sent') and @class='J-Ke n0']";
    protected final String recipientAreaXpath = "//textarea[@class='vO']";


    @FindBy(xpath = "//div[@jscontroller='eIu7Db']")
    private ButtonElement composeButton;
    @FindBy(xpath = recipientAreaXpath)
    private InputElement recipientArea;
    @FindBy(xpath = "//input[@class='aoT']")
    private InputElement topicArea;
    @FindBy(xpath = "//div[@class='Am Al editable LW-avf tS-tW']")
    private InputElement messageArea;
    @FindBy(xpath = "//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']")
    private ButtonElement sendButton;
    @FindBy(xpath = sentMsgTabXpath)
    private TabElement sentMessageTab;

    public GmailHomePage() {
        super();
    }
}
