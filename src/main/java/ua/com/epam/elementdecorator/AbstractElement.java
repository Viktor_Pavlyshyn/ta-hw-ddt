package ua.com.epam.elementdecorator;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import ua.com.epam.webdriver.LocalDriverManager;

import java.time.Duration;
import java.util.NoSuchElementException;

import static ua.com.epam.config.WaitTime.POLLING_EVERY_TIME;
import static ua.com.epam.config.WaitTime.WAIT_WITH_TIMEOUT;

public abstract class AbstractElement implements CustomElement {
    protected Wait<WebDriver> fwait;
    protected WebElement element;

    public AbstractElement(WebElement element) {
        this.element = element;
        this.fwait = setDriverToFluentWait(LocalDriverManager.getWebDriver());
    }

    public WebElement getWrappedElement() {
        return this.element;
    }

    public Wait<WebDriver> setDriverToFluentWait(WebDriver driver) {
        return new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(WAIT_WITH_TIMEOUT))
                .pollingEvery(Duration.ofSeconds(POLLING_EVERY_TIME))
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
    }
}
