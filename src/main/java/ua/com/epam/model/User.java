package ua.com.epam.model;

import lombok.Data;

@Data
public class User {
    private String login;
    private String password;
    private String recipient;
    private boolean result;
}
